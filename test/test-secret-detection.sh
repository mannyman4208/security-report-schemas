#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
SECRETS_SCHEMA="$PROJECT_DIRECTORY/dist/secret-detection-report-format.json"

source "$PROJECT_DIRECTORY/test/helper_functions.sh"
source "$PROJECT_DIRECTORY/test/common-tests.sh"

setup_suite() {
  regenerate_dist_schemas
}

test_secrets_contains_common_definitions() {
  ensure_common_definitions "$SECRETS_SCHEMA" '["secret_detection"]'
}

test_secrets_extensions() {
  verify_schema_contains_selector "$SECRETS_SCHEMA" 'select(.properties.vulnerabilities.items.required[] | contains("location"))'

  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.file"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.start_line"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.end_line"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.method"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.class"

  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.commit.properties.author"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.commit.properties.date"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.commit.properties.message"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.location.properties.commit.properties.sha"
  verify_schema_contains_selector "$SECRETS_SCHEMA" ".properties.vulnerabilities.items.properties.raw_source_code_extract"
}
