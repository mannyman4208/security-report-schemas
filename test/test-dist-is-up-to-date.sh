#!/bin/bash

PROJECT_DIRECTORY=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")
SAST_SCHEMA_FILE="$PROJECT_DIRECTORY/dist/sast-report-format.json"
DAST_SCHEMA_FILE="$PROJECT_DIRECTORY/dist/dast-report-format.json"
DS_SCHEMA_FILE="$PROJECT_DIRECTORY/dist/dependency-scanning-report-format.json"
CS_SCHEMA_FILE="$PROJECT_DIRECTORY/dist/container-scanning-report-format.json"
SECRETS_SCHEMA_FILE="$PROJECT_DIRECTORY/dist/secret-detection-report-format.json"
COVERAGE_FUZZING_SCHEMA_FILE="$PROJECT_DIRECTORY/dist/coverage-fuzzing-report-format.json"

read_schema() {
  SCHEMA_FILE="$2_SCHEMA_FILE"

  SCHEMA_CONTENTS="$(cat "${!SCHEMA_FILE}")"
  assert_equals 0, "$?", "Failed to read $2 schema"

  export "$1_$2_SCHEMA"="$SCHEMA_CONTENTS"
}

verify_schema_is_up_to_date() {
  BEFORE_SCHEMA="BEFORE_$1_SCHEMA"
  AFTER_SCHEMA="AFTER_$1_SCHEMA"
  MESSAGE="
The checked-in version of the $1 schema is different to what is generated for release.
This will cause problems for releasing, as releases are made using Git Tags.
Please refer to the README for setup, run ./scripts/distribute.sh on your local machine, and check-in the resulting dist/*.json files into Git.
"

  diff <(echo "${!BEFORE_SCHEMA}") <(echo "${!AFTER_SCHEMA}")
  assert_equals 0, "$?", "$MESSAGE"
}

test_dist_is_up_to_date() {
  read_schema "BEFORE" "SAST"
  read_schema "BEFORE" "DAST"
  read_schema "BEFORE" "DS"
  read_schema "BEFORE" "CS"
  read_schema "BEFORE" "SECRETS"
  read_schema "BEFORE" "COVERAGE_FUZZING"

  "$PROJECT_DIRECTORY"/scripts/distribute.sh >/dev/null
  assert_equals 0, "$?", "Failed to create schemas for distribution"

  read_schema "AFTER" "SAST"
  read_schema "AFTER" "DAST"
  read_schema "AFTER" "DS"
  read_schema "AFTER" "CS"
  read_schema "AFTER" "SECRETS"

  verify_schema_is_up_to_date "SAST"
  verify_schema_is_up_to_date "DAST"
  verify_schema_is_up_to_date "DS"
  verify_schema_is_up_to_date "CS"
  verify_schema_is_up_to_date "SECRETS"
}
