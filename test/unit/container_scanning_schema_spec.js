import b from './builders/index'
import {schemas} from './support/schemas'
import referenceCases from './support/reference_cases.json'

const baseLocation = {
  dependency: {
    package: {name: 'bzip2'},
    version: '1.0.6-8.1'
  },
  operating_system: 'debian:9',
  image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0'
}

describe('container scanning schema', () => {

  it('should validate location', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          default_branch_image: 'registry.com/product/webgoat-8.0:latest'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).success).toBeTruthy()
  })

  it('location dependency is required', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).errors[0].message).toContain('must have required property \'dependency\'')
  })

  it('location os is required', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).errors[0].message).toContain('must have required property \'operating_system\'')
  })

  it('location image is required', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).errors[0].message).toContain('must have required property \'image\'')
  })

  it('location dependency package is required', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          dependency: {
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          default_branch_image: 'registry.com/product/webgoat-8.0:latest'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).errors[0].message).toContain('must have required property \'package\'')
  })

  it('location dependency version is required', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          default_branch_image: 'registry.com/product/webgoat-8.0:latest'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).errors[0].message).toContain('must have required property \'version\'')
  })

  it('location dependency package name is required', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          dependency: {
            package: {noname: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0',
          default_branch_image: 'registry.com/product/webgoat-8.0:latest'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).errors[0].message).toContain('must have required property \'name\'')
  })

  it('location default_branch_image is optional', () => {
    const report = b.container_scanning.report({
      vulnerabilities: [b.container_scanning.vulnerability({
        location: {
          dependency: {
            package: {name: 'bzip2'},
            version: '1.0.6-8.1'
          },
          operating_system: 'debian:9',
          image: 'registry.com/product/webgoat-8.0@sha256:bc09fe2e0'
        }
      })]
    })

    expect(schemas.container_scanning.validate(report).success).toBeTruthy()
  })

  describe('default_branch_image validation', () => {
    referenceCases.default_branch_image.forEach(tc => {
      const { image, expected } = tc
      const mergedLocation = Object.assign(baseLocation, { default_branch_image: image })
      const report = b.container_scanning.report({
        vulnerabilities: [b.container_scanning.vulnerability({
          location: mergedLocation
        })]
      })

      it(`${expected ? 'accepts' : 'rejects'} ${image}`, () => {
        expect(schemas.container_scanning.validate(report).success).toEqual(expected)
      })
    })
  })

  describe('image validation', () => {
    referenceCases.image.forEach(tc => {
      const { image, expected } = tc
      const mergedLocation = Object.assign(baseLocation, { image })
      const report = b.container_scanning.report({
        vulnerabilities: [b.container_scanning.vulnerability({
          location: mergedLocation
        })]
      })

      it(`${expected ? 'accepts' : 'rejects'} ${image}`, () => {
        expect(schemas.container_scanning.validate(report).success).toEqual(expected)
      })
    })
  })
})
