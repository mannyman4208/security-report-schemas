import b from './builders/index'
import {schemas} from './support/schemas'

describe('dependency scanning schema', () => {

  it('should validate location', () => {
    const report = b.dependency_scanning.report({
      vulnerabilities: [b.dependency_scanning.vulnerability({
        location: {
          file: 'Gemfile.lock',
          dependency: {
            package: {name: 'rack'},
            version: '2.0.4',
            iid: 987654321,
            direct: true,
            dependency_path: [{iid: 123456789}]
          }
        },
      })]
    })

    expect(schemas.dependency_scanning.validate(report).success).toBeTruthy()
  })

  it('should validate dependency files', () => {
    const report = b.dependency_scanning.report({
      dependency_files: [{
        path: 'src/web.api/packages.lock.json',
        package_manager: 'nuget',
        dependencies: [{
          iid: 44,
          dependency_path: [{iid: 35}],
          package: {name: 'Antlr3.Runtime'},
          version: '3.5.1'
        }, {
          iid: 69,
          dependency_path: [{iid: 35}],
          package: {name: 'Iesi.Collections'},
          version: '4.0.4'
        }]
      }]
    })

    expect(schemas.dependency_scanning.validate(report).success).toBeTruthy()
  })

  it('requires dependency_files[].dependencies[].package', () => {
    const report = b.dependency_scanning.report({
      dependency_files: [{
        path: 'some/path',
        package_manager: 'some package manager',
        dependencies: [{iid: 1, version: '0.0.1'}]
      }]
    })

    expect(schemas.dependency_scanning.validate(report).errors[0].message).toContain('must have required property \'package\'')
  })

  it('requires dependency_files[].dependencies[].version', () => {
    const report = b.dependency_scanning.report({
      dependency_files: [{
        path: 'some/path',
        package_manager: 'some package manager',
        dependencies: [{iid: 1, package: 'foo'}]
      }]
    })

    expect(schemas.dependency_scanning.validate(report).errors[0].message).toContain('must have required property \'version\'')
  })

  it('requires dependency_files[].dependencies[].package.name', () => {
    const report = b.dependency_scanning.report({
      dependency_files: [{
        path: 'src/web.api/packages.lock.json',
        package_manager: 'nuget',
        dependencies: [{
          iid: 44,
          dependency_path: [{iid: 35}],
          package: {noname: 'Antlr3.Runtime'},
          version: '3.5.1'
        }]
      }]
    })

    expect(schemas.dependency_scanning.validate(report).errors[0].message).toContain('must have required property \'name\'')
  })

})
