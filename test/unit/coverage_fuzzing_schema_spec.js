import b from './builders/index'
import {schemas} from './support/schemas'

describe('coverage fuzzing schema', () => {

  it('should validate location', () => {
    const report = b.dast.report({
      vulnerabilities: [b.dast.vulnerability({
        location: {
          crash_type: 'Index-out-of-range',
          crash_state: 'file.Method.func7\nfile.Fuzz\n\n',
          stacktrace_snippet: 'panic: runtime error: index out of range'
        }
      })]
    })

    expect(schemas.dast.validate(report).success).toBeTruthy()
  })
})
