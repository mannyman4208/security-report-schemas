package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/xeipuuv/gojsonschema"
)

func main() {
	schema := gojsonschema.NewReferenceLoader("file://json-schema-spec/schema.json")
	document := loadStdin()

	result, err := gojsonschema.Validate(schema, document)
	if err != nil {
		log.Fatalf("Failed to validate schema: %s", err)
	}

	if !result.Valid() {
		fmt.Println("Schema has validation errors:")
		for _, msg := range result.Errors() {
			fmt.Printf("  - %s\n", msg.String())
		}
		os.Exit(1)
	}

	fmt.Println("Schema is valid.")
}

func loadStdin() gojsonschema.JSONLoader {
	var data map[string]interface{}
	err := json.NewDecoder(os.Stdin).Decode(&data)
	if err != nil {
		log.Fatalln(err)
	}
	return gojsonschema.NewGoLoader(data)
}
